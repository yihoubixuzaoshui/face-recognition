import cv2 as cv
import cv2


def face_detect_demo():
    #将图片转换为灰度图片
    gray=cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    #加载特征数据
    face_detector=cv.CascadeClassifier('D:\ANACONDA\Lib\site-packages\cv2\data\haarcascade_frontalface_default.xml')
    faces=face_detector.detectMultiScale(gray)
    for x,y,w,h in faces:
        cv.rectangle(img,(x,y),(x+w,y+h),color=(0,255,0),thickness=2)
    cv.imshow('result',img)




#加载图片
img=cv.imread('messi.jpg')
# 创建一个可调整大小的窗口
cv.namedWindow('result', cv.WINDOW_NORMAL)

# 设置窗口大小
cv.resizeWindow('result', 800, 600)  # 800x600 是你想要的窗口大小

face_detect_demo()

cv.waitKey(0)
cv.destroyAllWindows()
